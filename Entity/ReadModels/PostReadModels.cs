﻿using Fury.Core.Interface;
using System;
using System.Collections.Generic;

namespace Entity.ReadModels
{
    public class PostReadModels : IAggregateRoot
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string Thumbnail { get; set; }
        public string AuthorId { get; set; }
        public string AuthorEmail { get; set; }
        public IEnumerable<CategoryReadModels> Categories { get; set; }
    }
}
