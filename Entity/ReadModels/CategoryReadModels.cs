﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.ReadModels
{
    public class CategoryReadModels
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
