﻿using Appbackground.Mapper;
using Appbackground.SerializeEntity;
using Application.Command.Create;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Appbackground.Test.MapperTest
{
    public class PostSerializeMapperTest
    {
        [Fact]
        public void Should_Mapping_From_Post_Serialize_To_Create_Post_Command()
        {
            var category = new CategorySerialize() 
            {
                Id = Guid.NewGuid().ToString(),
                Name = ".NET"
            };

            var postSerialize = new PostSerialize()
            {
                Head = new Header
                {
                    Eventype = "POSTADDEDEVENT"
                },
                Body = new Body
                {
                    Id = Guid.NewGuid().ToString(),
                    Title = "post-title",
                    Content = "post-content",
                    Thumbnail = "http://localhost",
                    AuthorEmail = "abc@gmail.com",
                    AuthorId = Guid.NewGuid().ToString(),
                    Categories = new List<CategorySerialize> { category }
                }
            };

            var createCommandPost = AppBackgroundMapping.Mapper.Map<CreatePostCommand>(postSerialize);

            createCommandPost.Should().NotBeNull();
            createCommandPost.Id.Should().Be(postSerialize.Body.Id);
            createCommandPost.Title.Should().Be(postSerialize.Body.Title);
            createCommandPost.Thumbnail.Should().Be(postSerialize.Body.Thumbnail);
            createCommandPost.Content.Should().Be(postSerialize.Body.Content);
            createCommandPost.AuthorId.Should().Be(postSerialize.Body.AuthorId);
            createCommandPost.AuthorEmail.Should().Be(postSerialize.Body.AuthorEmail);
            createCommandPost.Categories.Should().ContainSingle();
            createCommandPost.Categories.Should().SatisfyRespectively(
                first =>
                {
                    first.Id.Should().Be(category.Id);
                    first.Name.Should().Be(category.Name);
                }
            );
        }
    }
}
