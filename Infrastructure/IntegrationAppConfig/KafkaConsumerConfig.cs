﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.IntegrationAppConfig
{
    public class KafkaConsumerConfig : BaseConfig
    {
        public string Topic { get; set; }
        public string GroupId { get; set; }
    }
}
