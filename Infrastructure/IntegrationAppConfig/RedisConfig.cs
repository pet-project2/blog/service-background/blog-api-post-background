﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.IntegrationAppConfig
{
    public class RedisConfig : BaseConfig
    {
        public string AbsoluteExpiredTime { get; set; }
        public string SlidingExpiredTime { get; set; }
    }
}
