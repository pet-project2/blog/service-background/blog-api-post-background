﻿using DataModel.AggregateModels;
using Infrastructure.Repository.CacheRepository;
using Infrastructure.Repository.SearchRepository;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.Repository
{
    public class WritePostRepository : IWritePostRepository<PostAggregate>
    {
        private readonly ICacheRepository<PostAggregate> _cacheRepository;
        private readonly ISearchRepository<PostAggregate> _searchRepository;
        public WritePostRepository(ICacheRepository<PostAggregate> cacheRepository, ISearchRepository<PostAggregate> searchRepository)
        {
            this._cacheRepository = cacheRepository;
            this._searchRepository = searchRepository;
        }

        public async Task AddAsync(PostAggregate entity, CancellationToken cancellationToken = default)
        {
            var postId = entity.Id;
            if(!string.IsNullOrEmpty(postId))
            {
                var authorId = entity.AuthorId;
                var postAuthorCacheKeys = !string.IsNullOrEmpty(entity.AuthorId) ? $"post_{postId}_author_{authorId}" : string.Empty;

                var arrayCacheKeys = new string[] { $"post_{postId}", postAuthorCacheKeys };

                await this._cacheRepository.SetWithMutipleMutipleKeyAsync(arrayCacheKeys, entity);
                await this._searchRepository.SetByIdAsync(postId, entity);
            }
        }

        public Task DeleteAsync(PostAggregate entity, CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }

        public Task DeleteRangeAsync(IEnumerable<PostAggregate> entities, CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }

        public Task SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }

        public Task UpdateAsync(PostAggregate entity, CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }
    }
}
