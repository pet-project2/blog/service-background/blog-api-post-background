﻿using DataModel.AggregateModels;
using Infrastructure.Repository.CacheRepository;
using Infrastructure.Repository.SearchRepository;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.Repository
{
    public class ReadPostRepository : IReadPostRepository<PostAggregate>
    {
        private readonly ICacheRepository<PostAggregate> _cacheRepository;
        private readonly ISearchRepository<PostAggregate> _searchRepository;
        public ReadPostRepository(ICacheRepository<PostAggregate> cacheRepository, ISearchRepository<PostAggregate> searchRepository)
        {
            this._cacheRepository = cacheRepository;
            this._searchRepository = searchRepository;
        }

        public Task<PostAggregate> GetByIdAsync(string id, CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<PostAggregate>> GetListAsync(CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }
    }
}
