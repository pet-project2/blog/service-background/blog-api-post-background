﻿using Fury.Core.Interface;
using System.Threading.Tasks;

namespace Infrastructure.Repository.SearchRepository
{
    public interface ISearchRepository<T> where T: class, IAggregateRoot
    {
        Task SetByIdAsync(string key, T value);
        Task DeleteByIdAsync(string key);
    }
}
