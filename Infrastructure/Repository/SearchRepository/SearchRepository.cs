﻿using DataModel.AggregateModels;
using Elasticsearch.Net;
using Microsoft.Extensions.Logging;
using Nest;
using System;
using System.Threading.Tasks;

namespace Infrastructure.Repository.SearchRepository
{
    public class SearchRepository : ISearchRepository<PostAggregate>
    {
        private readonly IElasticClient _elasticClient;
        private readonly ILogger<SearchRepository> _logger;
        public SearchRepository(IElasticClient elasticClient, ILogger<SearchRepository> logger)
        {
            this._elasticClient = elasticClient;
            this._logger = logger;
        }

        public Task DeleteByIdAsync(string key)
        {
            throw new NotImplementedException();
        }

        public async Task SetByIdAsync(string key, PostAggregate value)
        {
            if (string.IsNullOrEmpty(key))
            {
                await this._elasticClient.Indices.RefreshAsync();

                var searchResult = await this._elasticClient.GetAsync<PostAggregate>(key);
                var isSuccess = false;
                var message = string.Empty;
                if (searchResult.Source == null)
                {
                    var response = await this._elasticClient.IndexAsync(value, i => i.Id(key));
                    isSuccess = response.ApiCall.Success;
                    message = response.DebugInformation;
                }
                else
                {
                    var response = await this._elasticClient.UpdateAsync(DocumentPath<PostAggregate>.Id(key), u => u.Doc(value).Refresh(Refresh.WaitFor));
                    isSuccess = response.ApiCall.Success;
                    message = response.DebugInformation;
                }

                if (!isSuccess)
                {
                    this._logger.LogWarning(message);
                }
                else
                {
                    this._logger.LogInformation(message);
                }
            }
        }
    }
}
