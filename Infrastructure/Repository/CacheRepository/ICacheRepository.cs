﻿using Fury.Core.Interface;
using System.Threading.Tasks;

namespace Infrastructure.Repository.CacheRepository
{
    public interface ICacheRepository<T> where T : class, IAggregateRoot
    {
        Task SetWithMutipleMutipleKeyAsync(string[] arrKeys, T value);
        Task SetAsync(string key, T value);
        Task DeleteAsync(string key);
    }
}
