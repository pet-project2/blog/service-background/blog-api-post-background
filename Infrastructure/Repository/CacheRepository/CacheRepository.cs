﻿using DataModel.AggregateModels;
using Infrastructure.IntegrationAppConfig;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Infrastructure.Repository.CacheRepository
{
    public class CacheRepository : ICacheRepository<PostAggregate>
    {
        private readonly IDistributedCache _distributedCache;
        private readonly DistributedCacheEntryOptions _options;
        private readonly ILogger<CacheRepository> _logger;
        public CacheRepository(IDistributedCache distributedCache, IOptions<RedisConfig> cacheOptions, ILogger<CacheRepository> logger)
        {
            this._distributedCache = distributedCache;
            var absoluteExpiredTime = int.Parse(cacheOptions.Value.AbsoluteExpiredTime);
            var slidingExpiredTime = int.Parse(cacheOptions.Value.SlidingExpiredTime);
            this._options = new DistributedCacheEntryOptions()
            {
                AbsoluteExpirationRelativeToNow = TimeSpan.FromSeconds(absoluteExpiredTime),
                SlidingExpiration = TimeSpan.FromSeconds(slidingExpiredTime)
            };
            this._logger = logger;
        }

        public async Task DeleteAsync(string key)
        {
            if (!string.IsNullOrEmpty(key))
            {
                await this._distributedCache.RemoveAsync(key);
            }
        }

        public async Task SetAsync(string key, PostAggregate value)
        {
            try
            {
                if(!string.IsNullOrEmpty(key))
                {
                    var jsonStringValue = JsonSerializer.Serialize(value);
                    var valueEncode = Encoding.UTF8.GetBytes(jsonStringValue);
                    await this._distributedCache.SetAsync(key, valueEncode, this._options);
                    this._logger.LogInformation($"Set cache success with key : {key}");
                }
            }
            catch (Exception ex)
            {
                this._logger.LogError($"Can not set value to cache cause {ex.Message}");
            }
        }

        public async Task SetWithMutipleMutipleKeyAsync(string[] arrKeys, PostAggregate value)
        {
            try
            {
                if(arrKeys != null && arrKeys.Length > 0)
                {
                    foreach(var key in arrKeys)
                    {
                        await this.SetAsync(key, value);
                    }
                }
            }
            catch (Exception ex)
            {
                this._logger.LogError($"Can not set value to cache cause {ex.Message}");
            }
        }
    }
}
