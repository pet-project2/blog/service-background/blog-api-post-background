﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;

#nullable disable

namespace DataModel.EfModels
{
    public partial class CategoryEf
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid PostId { get; set; }
        public Guid CateId { get; set; }
        public virtual PostEf Post { get; set; }
    }
}