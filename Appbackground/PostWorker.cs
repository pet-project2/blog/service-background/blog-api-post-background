using Appbackground.Constants;
using Appbackground.Mapper;
using Appbackground.SerializeEntity;
using Application.Command.Create;
using Confluent.Kafka;
using Infrastructure.IntegrationAppConfig;
using MediatR;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace Appbackground
{
    public class PostWorker : BackgroundService
    {
        private readonly ILogger<PostWorker> _logger;
        private readonly IMediator _mediator;
        private readonly string _topic;
        private readonly IConsumer<string, string> _consumer;
        public PostWorker(ILogger<PostWorker> logger, IMediator mediator, IConsumer<string, string> consumer, IOptions<KafkaConsumerConfig> kafkaConsumerConfig)
        {
            this._logger = logger;
            this._mediator = mediator;
            this._consumer = consumer;
            this._topic = kafkaConsumerConfig.Value.Topic;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Worker running at: {time}", DateTimeOffset.Now);
            this._consumer.Subscribe(this._topic);
            while (!stoppingToken.IsCancellationRequested)
            {
                var consumeResult = this._consumer.Consume(stoppingToken);

                // serialze consumeResult
                var postDeserialize = TryDeserialize(consumeResult.Message.Value);
                if(postDeserialize != null)
                {
                    var evenType = postDeserialize.Head.Eventype;

                    switch (evenType.ToUpper())
                    {
                        case AppConstants.CREATE:
                            await this.CreatePostAsync(postDeserialize);
                            break;
                        case AppConstants.DELETE:
                            break;
                        case AppConstants.UPDATE:
                            break;
                        default:
                            break;
                    }

                    this._consumer.Commit(consumeResult);
                }
            }
        }

        protected async Task CreatePostAsync(PostSerialize message)
        {
            var createPostCommand = AppBackgroundMapping.Mapper.Map<CreatePostCommand>(message);
            await this._mediator.Send(createPostCommand);
        }

        private PostSerialize TryDeserialize(string message)
        {
            try
            {
                var jsonPostDeserialize = JsonSerializer.Deserialize<PostSerialize>(message);
                return jsonPostDeserialize;
            }
            catch (Exception ex)
            {
                this._logger.LogError($"Can not deserialize message cause: {ex.Message}");
                return null;
            }
        }
    }
}
