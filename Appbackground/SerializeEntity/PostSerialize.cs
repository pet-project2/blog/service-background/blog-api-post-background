﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Appbackground.SerializeEntity
{
    public class PostSerialize
    {
        public Header Head { get; set; }
        public Body Body { get; set; }
    }
}
