﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Appbackground.SerializeEntity
{
    public class Header
    {
        public string EventId { get; set; }
        public DateTime EventDate { get; set; }
        public string Eventype { get; set; }
    }
}
