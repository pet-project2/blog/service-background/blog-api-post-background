﻿using System.Collections.Generic;

namespace Appbackground.SerializeEntity
{
    public class CategorySerialize
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
    public class Body
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string Thumbnail { get; set; }
        public string AuthorId { get; set; }
        public string AuthorEmail { get; set; }
        public IEnumerable<CategorySerialize> Categories { get; set; }
    }
}
