﻿using Confluent.Kafka;
using Infrastructure.IntegrationAppConfig;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Appbackground.Extensions
{
    public static class ServiceKafkaExtensions
    {
        public static void AddKafkaConsumer<TKey, TValue>(this IServiceCollection services, IConfiguration configuration)
        {
            var kafkaConfig = configuration.GetSection("Kafka");
            var kafkaSettings = kafkaConfig.Get<KafkaConsumerConfig>();
            // add setting 
            services.Configure<KafkaConsumerConfig>(kafkaConfig);

            var consumerConfig = new ConsumerConfig
            {
                BootstrapServers = kafkaSettings.Uri,
                GroupId = kafkaSettings.GroupId,
                AutoOffsetReset = AutoOffsetReset.Latest,
                EnableAutoCommit = false
            };

            var consumerBuilder = new ConsumerBuilder<TKey, TValue>(consumerConfig).Build();
            services.AddSingleton(consumerBuilder);
        }
    }
}
