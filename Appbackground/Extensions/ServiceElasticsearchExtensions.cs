﻿using DataModel.AggregateModels;
using Infrastructure.IntegrationAppConfig;
using Infrastructure.Repository.SearchRepository;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Nest;
using System;

namespace Appbackground.Extensions
{
    public static class ServiceElasticsearchExtensions
    {
        public static void AddElasticsearch(this IServiceCollection services, IConfiguration configuration)
        {
            // set config elasticsearch
            var esSetting = configuration.GetSection("Elasticsearch").Get<ElasticsearchConfig>();
            var settings = new ConnectionSettings(new Uri(esSetting.Uri))
                               .EnableDebugMode()
                               .DefaultIndex(esSetting.Index);

            var client = new ElasticClient(settings);
            esSetting.AddPostMapping(client); // add post Mapping

            services.AddSingleton<IElasticClient>(client);
            services.AddScoped<ISearchRepository<PostAggregate>, SearchRepository>();
        }
    }
}
