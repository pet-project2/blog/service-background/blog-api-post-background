﻿using DataModel.AggregateModels;
using Infrastructure.IntegrationAppConfig;
using Infrastructure.Repository.CacheRepository;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Appbackground.Extensions
{
    public static class ServiceRedisExtensions
    {
        public static void AddRedis(this IServiceCollection services, IConfiguration configuration)
        {
            var redisConfig = configuration.GetSection("Redis");
            var redisSettings = redisConfig.Get<RedisConfig>();

            // add setting 
            services.Configure<RedisConfig>(redisConfig);

            services.AddStackExchangeRedisCache(options =>
            {
                options.Configuration = $"{redisSettings.Uri},password={redisSettings.Password}";
            });

            services.AddScoped<ICacheRepository<PostAggregate>, CacheRepository>();
        }
    }
}
