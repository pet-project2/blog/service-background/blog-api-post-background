﻿using Appbackground.SerializeEntity;
using Application.Dto;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Appbackground.Mapper
{
    public class CategorySerializeMapper : Profile
    {
        public CategorySerializeMapper()
        {
            CreateMap<CategorySerialize, CategoryDto>()
                .ForMember(d => d.Id, s => s.MapFrom(src => src.Id))
                .ForMember(d => d.Name, s => s.MapFrom(src => src.Name));
        }
    }
}
