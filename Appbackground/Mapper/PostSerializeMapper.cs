﻿using Appbackground.SerializeEntity;
using Application.Command.Create;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Appbackground.Mapper
{
    public class PostSerializeMapper : Profile
    {
        public PostSerializeMapper()
        {
            CreateMap<PostSerialize, CreatePostCommand>()
                .ForMember(d => d.Id, s => s.MapFrom(src => src.Body.Id))
                .ForMember(d => d.Title, s => s.MapFrom(src => src.Body.Title))
                .ForMember(d => d.Content, s => s.MapFrom(src => src.Body.Content))
                .ForMember(d => d.Thumbnail, s => s.MapFrom(src => src.Body.Thumbnail))
                .ForMember(d => d.AuthorId, s => s.MapFrom(src => src.Body.AuthorId))
                .ForMember(d => d.AuthorEmail, s => s.MapFrom(src => src.Body.AuthorEmail))
                .ForMember(d => d.Categories, s => s.MapFrom(src => src.Body.Categories));
        }
    }
}
