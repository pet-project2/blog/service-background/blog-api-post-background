﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Appbackground.Constants
{
    public struct AppConstants
    {
        public const string CREATE = "POSTADDEDEVENT";
        public const string UPDATE = "POSTUPDATEEVENT";
        public const string DELETE = "POSTDELETEEVENT";
    }
}
