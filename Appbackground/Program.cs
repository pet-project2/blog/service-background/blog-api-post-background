using Appbackground.Extensions;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Reflection;

namespace Appbackground
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureServices((hostContext, services) =>
                {
                    var configuration = hostContext.Configuration;
                    services.AddElasticsearch (configuration);
                    services.AddKafkaConsumer<string, string>(configuration);
                    services.AddRedis(configuration);
                    // MediaR
                    var assemblies = new Assembly[]
                    {
                        AppDomain.CurrentDomain.Load("Infrastructure"),
                        AppDomain.CurrentDomain.Load("Application")
                    };

                    services.AddMediatR(assemblies);
                    services.AddAutoMapper(assemblies);
                    services.AddHostedService<PostWorker>();
                });
    }
}
