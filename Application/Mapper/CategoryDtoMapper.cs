﻿using Application.Dto;
using AutoMapper;
using DataModel.AggregateModels;

namespace Application.Mapper
{
    public class CategoryDtoMapper : Profile
    {
        public CategoryDtoMapper()
        {
            CreateMap<CategoryDto, Category>()
                .ForMember(d => d.Id, s => s.MapFrom(src => src.Id))
                .ForMember(d => d.Name, s => s.MapFrom(src => src.Name));
        }
    }
}
