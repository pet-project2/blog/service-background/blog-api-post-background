﻿using Application.Command.Create;
using AutoMapper;
using DataModel.AggregateModels;

namespace Application.Mapper
{
    public class CreatePostCommandMapper : Profile
    {
        public CreatePostCommandMapper()
        {
            CreateMap<CreatePostCommand, PostAggregate>()
                .ForMember(d => d.Id, s => s.MapFrom(src => src.Id))
                .ForMember(d => d.Title, s => s.MapFrom(src => src.Title))
                .ForMember(d => d.Content, s => s.MapFrom(src => src.Content))
                .ForMember(d => d.Thumbnail, s => s.MapFrom(src => src.Thumbnail))
                .ForMember(d => d.AuthorId, s => s.MapFrom(src => src.AuthorId))
                .ForMember(d => d.AuthorEmail, s => s.MapFrom(src => src.AuthorEmail))
                .ForMember(d => d.Categories, s => s.MapFrom(src => src.Categories));
        }
    }
}
