﻿using Application.Dto;
using MediatR;
using System.Collections.Generic;

namespace Application.Command.Create
{
    public class CreatePostCommand : IRequest
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string Thumbnail { get; set; }
        public string AuthorId { get; set; }
        public string AuthorEmail { get; set; }
        public IEnumerable<CategoryDto> Categories { get; set; }
    }
}
