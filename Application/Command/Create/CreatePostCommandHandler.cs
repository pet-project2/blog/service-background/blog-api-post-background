﻿using Application.Exceptions;
using Application.Mapper;
using DataModel.AggregateModels;
using Infrastructure.Repository;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Command.Create
{
    public class CreatePostCommandHandler : IRequestHandler<CreatePostCommand, Unit>
    {
        private readonly IReadPostRepository<PostAggregate> _readPostRepository;
        private readonly IWritePostRepository<PostAggregate> _writePostRepository;
        private readonly ILogger<CreatePostCommand> _logger;
        public CreatePostCommandHandler(IServiceScopeFactory serviceScopeFactory, ILogger<CreatePostCommand> logger)
        {
            this._readPostRepository = serviceScopeFactory.CreateScope().ServiceProvider.GetService<IReadPostRepository<PostAggregate>>();
            this._writePostRepository = serviceScopeFactory.CreateScope().ServiceProvider.GetService<IWritePostRepository<PostAggregate>>();
            this._logger = logger;
        }
        public async Task<Unit> Handle(CreatePostCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var key = request.Id;
                if (string.IsNullOrEmpty(key))
                    throw new CreatePostCommandHandlerException("Can not set key cause key is null or empty");

                var postValue = ApplicationMapping.Mapper.Map<PostAggregate>(request);
                await this._writePostRepository.AddAsync(postValue);
            }
            catch (Exception ex)
            {
                this._logger.LogError(ex.Message);
            }

            return await Task.FromResult(Unit.Value);

        }
    }
}
